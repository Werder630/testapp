package com.example.aynetyaga.mathkeeper.net.request;

import org.ksoap2.serialization.SoapObject;

import com.example.aynetyaga.mathkeeper.utils.Constants;

/**
 * Created by Юлия on 19.03.2016.
 */
public abstract class BaseRequest {
    private String methodName;

    public BaseRequest(String methodName) {
        this.methodName = methodName;
    }

    public String getMethodName() {
        return methodName;
    }

    protected SoapObject createSoapObject() {
        return new SoapObject(Constants.INSTANCE.getNAMESPACE(), methodName);
    }

    public abstract SoapObject getSoapObject();

    public abstract Class getResponseClass();

}