package com.example.aynetyaga.mathkeeper.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by AYNetyaga on 12.10.15.
 * Rucard LTD
 */
public class LoginResponse {
    @SerializedName("isError")
    public boolean isError;
    @SerializedName("errorText")
    public String errorText;
    @SerializedName("user")
    public User userResponse;

    public static class User {
        @SerializedName("id")
        public String userId;
        @SerializedName("name")
        public String userName;
    }
}
