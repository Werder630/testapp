package com.example.aynetyaga.mathkeeper.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.aynetyaga.testapp.R;

import java.util.Collections;
import java.util.List;

import com.example.aynetyaga.mathkeeper.net.response.ExersiceResponse.Exersice;
import com.example.aynetyaga.mathkeeper.utils.NaturalOrderComparator;

/**
 * Created by AYNetyaga on 27.03.15.
 * Rucard LTD
 */
public class ExercisesAdapter extends ArrayAdapter<Exersice> {

    private Context context;
    private List<Exersice> list;
    private int totalSum;
    private ClickItemListener listener;;

    public ExercisesAdapter(Context context, List<Exersice> list) {
        super(context, R.layout.exercise_row, list);
        this.context = context;
        this.list = list;
    }

    public void setClickItemListener(ClickItemListener listener) {
        this.listener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if(convertView == null)  {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.exercise_row, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.cb = (CheckBox) convertView.findViewById(R.id.exercise_checkbox);
            viewHolder.tv = (TextView) convertView.findViewById(R.id.exercise_textview_price);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.cb.setText(list.get(position).exer_id);
        if (list.get(position).selected)  {
            viewHolder.cb.setChecked(true);
        } else {viewHolder.cb.setChecked(false);}

        viewHolder.cb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.get(position).selected = (((CheckBox) view).isChecked());
                updateTotalSum();
                if (listener != null) listener.click();
            }
        });

        viewHolder.tv.setText(list.get(position).price +" руб.");

        return convertView;
    }

    @Override
    public Exersice getItem(int position) {
       return list.get(position);
    }

    public Integer getTotalSum() {
        return totalSum;
    }

    private void updateTotalSum() {
        totalSum = 0;
        for (Exersice exersice : list)
            if (exersice.selected) totalSum += exersice.price;
    }

    public void selectAll() {
        for (Exersice exersice : list)
            exersice.selected = true;

        notifyDataSetChanged();
        updateTotalSum();
    }

    public String getPrintListSelectedExercises()  {
        StringBuilder sb = new StringBuilder();

        Collections.sort(list, new NaturalOrderComparator());

        for (int i = 0; i < list.size(); i++) {
            if (i != (list.size()-1))  {
                if (list.get(i).selected) sb.append(list.get(i).exer_id +", ");
            }
                else {  if (list.get(i).selected) sb.append(list.get(i));}
        }
        return sb.toString();
    }

    static class ViewHolder {
        CheckBox cb;
        TextView tv;
    }

    public interface ClickItemListener {
        void click();
    }

}