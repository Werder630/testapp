package com.example.aynetyaga.mathkeeper.net;

import android.os.AsyncTask;

import com.example.aynetyaga.mathkeeper.net.request.BaseRequest;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

import com.example.aynetyaga.mathkeeper.utils.Constants;

/**
 * Created by AYNetyaga on 04.08.15.
 * Rucard LTD
 */
public class MyAsyncTask extends AsyncTask<Void, Void, SoapObject> {

    private BaseRequest request;

    MyAsyncTask(BaseRequest baseRequest) {
        this.request = baseRequest;
    }

    @Override
    protected SoapObject doInBackground(Void... params) {
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;

        envelope.setOutputSoapObject(request.getSoapObject());
        HttpTransportSE androidHttpTransport = new HttpTransportSE(Constants.INSTANCE.getURL());

        try {
            androidHttpTransport.call(Constants.INSTANCE.getSOAP_ACTION() + request.getMethodName(), envelope);
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
            return null;
        }

        return (SoapObject) envelope.bodyIn;
    }
}
