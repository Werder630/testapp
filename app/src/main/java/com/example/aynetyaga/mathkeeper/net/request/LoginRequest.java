package com.example.aynetyaga.mathkeeper.net.request;

import com.example.aynetyaga.mathkeeper.net.response.LoginResponse;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by AYNetyaga on 20.10.15.
 * Rucard LTD
 */
public class LoginRequest extends BaseRequest {
    public String login;
    public String password;

    public LoginRequest() {
        super("Login");
    }

    @Override
    public SoapObject getSoapObject() {
        SoapObject object = createSoapObject();
        object.addProperty("login", login);
        object.addProperty("password", password);
        return object;
    }

    @Override
    public Class getResponseClass() {
        return LoginResponse.class;
    }
}
