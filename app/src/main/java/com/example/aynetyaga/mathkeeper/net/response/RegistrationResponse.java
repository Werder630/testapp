package com.example.aynetyaga.mathkeeper.net.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Юлия on 20.03.2016.
 */
public class RegistrationResponse  {
    @SerializedName("isError")
    public boolean isError;
    @SerializedName("errorText")
    public String errorText;
}
