package com.example.aynetyaga.mathkeeper.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AYNetyaga on 07.10.15.
 * Rucard LTD
 */
public class VariantResponse  {
    @SerializedName("variantList")
    public List<Variant> variants;

    public static class Variant {
        @SerializedName("variant")
        public int idVariant;

        @Override
        public String toString() {
            return "Вариант "+ idVariant;
        }
    }
}
