package com.example.aynetyaga.mathkeeper.ui

import android.app.AlertDialog
import android.app.ListActivity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.aynetyaga.testapp.R
import com.example.aynetyaga.mathkeeper.net.RequestWebServiceHelper
import com.example.aynetyaga.mathkeeper.net.response.MethodResponse
import com.example.aynetyaga.mathkeeper.utils.DownloaderMethod




class MethodActivity() : ListActivity() {
    val INTENT_KEY_METHOD_ID = "MethodActivity.MethodId"
    val INTENT_KEY_METHOD_NAME = "MethodActivity.MethodName"

    private var methodResponse: MethodResponse? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setTitle("Выберите тему");

        methodResponse = RequestWebServiceHelper(this).methodList;
        if (methodResponse != null && (methodResponse as MethodResponse).methods != null && (methodResponse as MethodResponse).methods.size > 0) {
            setListAdapter(MethodAdapter(this, (methodResponse as MethodResponse).methods))
        }
    }

    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        val intent = Intent(this, VariantActivity::class.java)
        intent.putExtra(INTENT_KEY_METHOD_ID, methodResponse!!.methods[position].id)
        intent.putExtra(INTENT_KEY_METHOD_NAME, methodResponse!!.methods[position].name)
        startActivity(intent)
    }

    private fun getDownloadMethodDialog(method: MethodResponse.Method) {
        AlertDialog.Builder(this)
                .setMessage("Скачать методичку по теме " + method.name + "?")
        .setPositiveButton("Да", { dialogInterface, i ->
            DownloaderMethod(this).execute(method.linkToFile, method.name)
        })
        .setNegativeButton("Отмена", { dialogInterface, i -> dialogInterface.dismiss() })
        .create().show()
    }

    private class MethodAdapter(context: Context, val list: List<MethodResponse.Method>)
        : ArrayAdapter<MethodResponse.Method>(context, R.layout.methods_row, list) {

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
            val method = list[position]
            if (convertView == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val convertedView = inflater.inflate(R.layout.methods_row, parent, false)
                convertedView.findViewById(R.id.methods_download).setOnClickListener {
                    (context as MethodActivity).getDownloadMethodDialog(method)
                }
                (convertedView.findViewById(R.id.methods_text) as TextView).text = method.name
                return convertedView
            } else {
                return convertView
            }
        }
    }
}