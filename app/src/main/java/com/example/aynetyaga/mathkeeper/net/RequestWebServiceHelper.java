package com.example.aynetyaga.mathkeeper.net;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Looper;

import com.example.aynetyaga.mathkeeper.utils.NaturalOrderComparator;

import com.example.aynetyaga.mathkeeper.net.request.BaseRequest;
import com.example.aynetyaga.mathkeeper.net.request.ExerciseRequest;
import com.example.aynetyaga.mathkeeper.net.request.LoginRequest;
import com.example.aynetyaga.mathkeeper.net.request.MethodRequest;
import com.example.aynetyaga.mathkeeper.net.request.RegistrationRequest;
import com.example.aynetyaga.mathkeeper.net.request.VariantRequest;
import com.example.aynetyaga.mathkeeper.net.response.ExersiceResponse;
import com.example.aynetyaga.mathkeeper.net.response.LoginResponse;
import com.example.aynetyaga.mathkeeper.net.response.MethodResponse;
import com.example.aynetyaga.mathkeeper.net.response.RegistrationResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.Collections;

import com.example.aynetyaga.mathkeeper.net.response.VariantResponse;

public class RequestWebServiceHelper {

    private Context context;
    private ProgressDialog pd;

    public RequestWebServiceHelper(Context context) {
        this.context = context;
    }

    public RegistrationResponse registration(String name, String login, String password) {
        RegistrationRequest request = new RegistrationRequest();
        request.name = name;
        request.password = password;
        request.login = login;

        return getResultFromServer(request, "Регистрация профиля");
    }

    public LoginResponse getLoginResponse(String login, String password) {
        LoginRequest request = new LoginRequest();
        request.login = login;
        request.password = password;

        return getResultFromServer(request, "Выполнятеся авторизация на сервере");
    }

    // Получение списка методичек
    public MethodResponse getMethodList() {
        MethodRequest request = new MethodRequest();

        return getResultFromServer(request, "Получение списка методичек");
    }

    // Получение списка вариантов
    public VariantResponse getVariantsListByMethodId(Integer id_method) {
        VariantRequest request = new VariantRequest();
        request.methodId = String.valueOf(id_method);

        VariantResponse response = getResultFromServer(request, "Получение списка вариантов");
        if (response != null && response.variants != null) Collections.sort(response.variants, new NaturalOrderComparator());
        return getResultFromServer(request, "Получение списка вариантов");
    }

    // Получение списка решений
    public ExersiceResponse getExercisesListByVariant(int id_method, int variant) {
        ExerciseRequest request = new ExerciseRequest(context);
        request.idMethod = id_method;
        request.variant = variant;

        return getResultFromServer(request, "Получение решений");
    }

/*    // Сохранение отчета о покупке
    public CreationOrderResponse getExercisesListByVariant(int id_method, int variant) {
        ExerciseRequest request = new ExerciseRequest(context);
        request.idMethod = id_method;
        request.variant = variant;

        return getResultFromServer(request, "Получение решений");*/


    private <T> T getResultFromServer(BaseRequest request, String dialogMessage) {
        SoapObject resultsRequestSOAP = null;
        showProgressDialog(dialogMessage);
        try {
            resultsRequestSOAP = new MyAsyncTask(request).execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (pd != null) pd.dismiss();

        String resultJSON = null;
        if (resultsRequestSOAP != null)
            resultJSON = resultsRequestSOAP.getProperty(request.getMethodName() + "Result").toString();

        if (resultJSON == null) return null;
        T result = null;

        try {
            JSONObject array = new JSONObject(resultJSON);
            Gson gson = new GsonBuilder().create();
            result = (T) gson.fromJson(array.toString(), request.getResponseClass());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return result;
    }

    private void showProgressDialog() {
        showProgressDialog("Выполняется запрос к серверу...");
    }

    private void showProgressDialog(final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                pd = ProgressDialog.show(context, "Подождите", s);
                Looper.loop();
            }
        }).start();
    }
}
