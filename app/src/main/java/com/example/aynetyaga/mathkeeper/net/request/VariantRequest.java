package com.example.aynetyaga.mathkeeper.net.request;

import org.ksoap2.serialization.SoapObject;

import com.example.aynetyaga.mathkeeper.net.response.VariantResponse;

/**
 * Created by Юлия on 03.03.2016.
 */
public class VariantRequest extends BaseRequest {
    public String methodId;

    public VariantRequest() {
        super("GetVariants");
    }

    @Override
    public SoapObject getSoapObject() {
        SoapObject object = createSoapObject();
        object.addProperty("id_method", methodId);

        return object;
    }

    @Override
    public Class getResponseClass() {
        return VariantResponse.class;
    }
}
