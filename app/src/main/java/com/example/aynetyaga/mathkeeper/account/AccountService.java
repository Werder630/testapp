package com.example.aynetyaga.mathkeeper.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.aynetyaga.mathkeeper.account.AccountAuthenticator;

/**
 * Created by AYNetyaga on 11.06.15.
 * Rucard LTD
 */
public class AccountService extends Service {

    AccountAuthenticator account;

    @Override
    public void onCreate() {
        super.onCreate();
        account = new AccountAuthenticator(getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return account.getIBinder();
    }


}
