package com.example.aynetyaga.mathkeeper.ui

import android.content.DialogInterface
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.aynetyaga.mathkeeper.net.RequestWebServiceHelper
import com.example.aynetyaga.mathkeeper.net.response.ExersiceResponse
import com.example.aynetyaga.mathkeeper.utils.FTPHelperManager
import com.example.aynetyaga.mathkeeper.utils.MessageHelper
import com.example.aynetyaga.mathkeeper.utils.NaturalOrderComparator
import com.example.aynetyaga.testapp.R
import kotlinx.android.synthetic.main.activity_exercise.*
import java.io.File
import java.util.*

/**
* Created by Юлия on 02.04.2016.
*/
class ExerciseActivityK : AppCompatActivity(), ExercisesAdapter.ClickItemListener {

    var currentMethod: Int? = null
    var currentVariant: Int? = null

    private var exerciseList: List<ExersiceResponse.Exersice>? = null
    private var adapter : ExercisesAdapter? = null
    private var menu : Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_exercise)

        currentMethod = intent.getIntExtra(MethodActivity().INTENT_KEY_METHOD_ID, -1)
        currentVariant = intent.getIntExtra(VariantActivity().VARIANT, -1)

        title = "Вариант $currentVariant"
        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)

        exerciseList = RequestWebServiceHelper(this).getExercisesListByVariant(currentMethod as Int, currentVariant as Int).exersiceList
        Collections.sort(exerciseList, NaturalOrderComparator())

        adapter = ExercisesAdapter(this, exerciseList)
        adapter?.setClickItemListener(this)
        exercise_list.adapter = adapter
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_exercise, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
            }

            R.id.exercise_menu_selectall -> {
                adapter?.selectAll()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    @Suppress("unused")
    fun onCreateOrderButtonClick(view : View) {
        if ((adapter != null) && ((adapter as ExercisesAdapter).totalSum > 0))
            showOrderDialog()
        else
            MessageHelper(this).showWarningToast("Не выбрано ни одного задания")
    }

    private fun showOrderDialog() {
        AlertDialog.Builder(this)
            .setTitle("Данные о заказе")
            .setMessage("Выбраны номера: \n" +
                    " " + adapter?.getPrintListSelectedExercises() +
                    " на общую сумму " + adapter?.getTotalSum() +
                    " руб. \n Сформировать заказ?")
            .setPositiveButton("Да", { dialogInterface, i -> createOrder(exerciseList as List<ExersiceResponse.Exersice>) })
            .setNegativeButton("Отмена", { dialogInterface, i -> dialogInterface.dismiss() })
            .create().show()
    }

    private fun createOrder(list: List<ExersiceResponse.Exersice>) {
        val manager = FTPHelperManager()
        manager.setDownloadListener { showFinishDialog() }
        var selectedExercises = list.filter { it.selected == true }.map { it.exer_id }
        manager.downloadFiles(this, currentMethod as Int, currentVariant as Int, selectedExercises)
    }

    fun getFolderPath(): String {
        val theDir = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath
                + "/MathKeeper/TR " + currentMethod + "/Variant " + currentVariant)

        return theDir.getAbsolutePath()
    }

    internal fun showFinishDialog() {
        AlertDialog.Builder(this)
                .setTitle("Скачивание завершено успешно")
                .setIcon(R.mipmap.complete_icon)
                .setMessage("Выбранные примеры скачаны в папку: \n" + getFolderPath())
                .setPositiveButton("Ok") { dialog, which -> }
                .create().show()
    }

    override fun click() {
        (menu?.getItem(0) as MenuItem).setTitle(adapter?.getTotalSum().toString() + " руб.")
    }
}
