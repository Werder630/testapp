package com.example.aynetyaga.mathkeeper.ui

import android.accounts.Account
import android.accounts.AccountAuthenticatorActivity
import android.accounts.AccountManager
import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.example.aynetyaga.mathkeeper.account.AccountAuthenticator
import com.example.aynetyaga.testapp.R
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONException
import com.example.aynetyaga.mathkeeper.net.RequestWebServiceHelper
import com.example.aynetyaga.mathkeeper.utils.Constants
import com.example.aynetyaga.mathkeeper.utils.MessageHelper

class LoginActivity : AccountAuthenticatorActivity() {
    private val messageHelper = MessageHelper(this);

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login);

        if (isAccountExists()) {
            startActivity(Intent(this, MenuActivity::class.java));
            finish();
        }
    }

    @Throws(JSONException::class)
    fun login(l: String, p: String) {
        val answer = RequestWebServiceHelper(this).getLoginResponse(l, p)
        if (answer.isError) {
            messageHelper.showWarningToast(answer.errorText)
        } else {
            messageHelper.showSuccessToast("Авторизация прошла успешно!")
            val intent = Intent(this, MenuActivity::class.java)
            val result = addAccount(Integer.parseInt(answer.userResponse.userId), answer.userResponse.userName, l, p)
            setAccountAuthenticatorResult(result.extras)
            startActivity(intent)
            finish()
        }
    }

    private fun addAccount(id: Int, name: String, login: String, password: String): Intent {
        val account = Account(name, AccountAuthenticator.ACCOUNT_NAME)
        val am = AccountManager.get(this)
        val userData = Bundle()
        userData.putString(Constants.ACCOUNT_ID, id.toString())
        userData.putString(Constants.ACCOUNT_LOGIN, login)
        am.addAccountExplicitly(account, password, userData)
        val accountCreationResult = Intent()
        accountCreationResult.putExtra(AccountManager.KEY_ACCOUNT_NAME, account.name)
        accountCreationResult.putExtra(AccountManager.KEY_ACCOUNT_TYPE, AccountAuthenticator.ACCOUNT_NAME)
        return accountCreationResult
    }

    private fun isAccountExists(): Boolean {
        val accountManager = AccountManager.get(this)
        val accounts = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_NAME)
        return accounts.size > 0
    }

    @Suppress("unused")
    fun onContinueButtonClick(@Suppress("UNUSED_PARAMETER") view: View) {
        if (login_textview.text.isEmpty() || password_textview.text.isEmpty()) {
            messageHelper.showWarningToast("Все поля должны быть заполнены!")
        } else {
            try {
                login(login_textview.text.toString(), password_textview.text.toString())
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
    }

    @Suppress("unused")
    fun onRegistrationDialog(@Suppress("UNUSED_PARAMETER") view: View) {
        val contentView = layoutInflater.inflate(R.layout.registration_dialog, null)
        val name = contentView.findViewById(R.id.name) as EditText
        val login = contentView.findViewById(R.id.login) as EditText
        val password = contentView.findViewById(R.id.password) as EditText

        val ad = AlertDialog.Builder(this)
                .setTitle("Регистрация в приложении")
                .setView(contentView)
                .setPositiveButton("Готово", { dialog, which ->
                    val response = RequestWebServiceHelper(this).registration(name.text.toString(), login.text.toString(), password.text.toString());
                    if (response.isError) {
                        messageHelper.showWarningToast(response.errorText)
                    } else {
                        messageHelper.showSuccessToast("Регистрация выполнена успешно \n Войдите в систему под своим логином")
                    }
                })
                .setNegativeButton("Отмена", { dialog, which -> dialog.cancel() })
                .setCancelable(false)
                .create()

        ad.show()
    }
}
