package com.example.aynetyaga.mathkeeper.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AYNetyaga on 07.10.15.
 * Rucard LTD
 */
public class MethodResponse {
        @SerializedName("methodList")
        public List<Method> methods;

        public static class Method {
                @SerializedName("id")
                public Integer id;
                @SerializedName("name")
                public String name;
                @SerializedName("url")
                public String linkToFile;
        }
}
