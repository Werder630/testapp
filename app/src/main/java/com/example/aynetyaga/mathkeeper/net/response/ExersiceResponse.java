package com.example.aynetyaga.mathkeeper.net.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AYNetyaga on 08.10.15.
 * Rucard LTD
 */
public class ExersiceResponse  {
    @SerializedName("exerciseList")
    public List<Exersice> exersiceList;

    public static class Exersice {
        @SerializedName("id")
        public String exer_id;
        @SerializedName("price")
        public int price;
        @SerializedName("isPaid")
        public boolean isPaid;
        public boolean selected;

        @Override
        public String toString() {
            return exer_id;
        }
    }
}

