package com.example.aynetyaga.mathkeeper.ui

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.aynetyaga.mathkeeper.account.AccountAuthenticator
import com.example.aynetyaga.testapp.R
import kotlinx.android.synthetic.main.activity_profile.*
import com.example.aynetyaga.mathkeeper.utils.Constants


class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val acc = account
        val accountManager = AccountManager.get(this)
        if (acc != null) {
            profile_id.text = accountManager.getUserData(acc, Constants.ACCOUNT_ID)
            profile_name.text = acc.name
            profile_login.text = accountManager.getUserData(acc, Constants.ACCOUNT_LOGIN)
        }
    }

    fun onExitButtonClick(view: View) {
        removePreviousAccount()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    private val account: Account?
        get() {
            val accountManager = AccountManager.get(this)
            val accounts = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_NAME)
            if (accounts.size > 0) {
                return accounts[0]
            }
            return null
        }

    private fun removePreviousAccount() {
        val accountManager = AccountManager.get(this)
        val accounts = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_NAME)
        if (accounts.size > 0) {
            accountManager.removeAccount(accounts[0], null, null)
        }
    }

}
