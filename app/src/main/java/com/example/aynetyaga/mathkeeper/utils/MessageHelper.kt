package com.example.aynetyaga.mathkeeper.utils

import android.R
import android.content.Context
import android.graphics.Color
import android.widget.TextView
import android.widget.Toast

/**
 * Created by Юлия on 19.03.2016.
 */
class MessageHelper(val context : Context) {

    fun showWarningToast(message: String) {
        showInformationToast(message, Color.parseColor("#FF7373"))
    }

    fun showSuccessToast(message: String) {
        showInformationToast(message, Color.parseColor("#67E667"))
    }

    fun showInformationToast(text: String, background: Int) {
        var t = Toast.makeText(context, text, Toast.LENGTH_LONG)
        val v = t.getView()
        v.setBackgroundColor(background)
        (v.findViewById(R.id.message) as TextView).setTextColor(Color.BLACK)
        t.show()
    }
}