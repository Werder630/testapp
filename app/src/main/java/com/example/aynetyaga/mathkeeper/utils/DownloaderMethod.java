package com.example.aynetyaga.mathkeeper.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by AYNetyaga on 07.10.15.
 * Rucard LTD
 */
public class DownloaderMethod extends AsyncTask<String, String, Void> {
    private static final int MEGABYTE = 1024 * 1024;

    private ProgressDialog pDialog;

    public DownloaderMethod(Context context) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Файл скачавается. Подождите...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog.show();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        pDialog.setProgress(Integer.parseInt(values[0]));
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        pDialog.dismiss();
    }

    @Override
    protected Void doInBackground(String... params) {
        try {
            URL url = new URL(params[0]);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            FileOutputStream fileOutputStream = new FileOutputStream(Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    + "/" + params[1] + ".pdf");
            int totalSize = urlConnection.getContentLength();
            byte[] buffer = new byte[MEGABYTE];
            int bufferLength;
            int total = 0;

            while ((bufferLength = inputStream.read(buffer)) > 0) {
                total += bufferLength;
                publishProgress("" + ((total * 100) / totalSize));
                fileOutputStream.write(buffer, 0, bufferLength);
            }
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}

