package com.example.aynetyaga.mathkeeper.ui

import android.R
import android.app.ListActivity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.TextView
import com.example.aynetyaga.mathkeeper.net.RequestWebServiceHelper
import com.example.aynetyaga.mathkeeper.net.response.VariantResponse

/**
 * Created by Юлия on 15.03.2016.
 */
class VariantActivity : ListActivity() {
    val VARIANT = "VariantActivity.Variant"

    private var variants : List<VariantResponse.Variant>? = null

    override fun onCreate(savedInstanceState : Bundle?){
        super.onCreate(savedInstanceState)

        variants = RequestWebServiceHelper(this).getVariantsListByMethodId(intent.getIntExtra(MethodActivity().INTENT_KEY_METHOD_ID, -1)).variants;

        if (variants != null && variants?.isNotEmpty() as Boolean) {
            listAdapter = ArrayAdapter(this, R.layout.simple_list_item_1, variants)
        } else {
            val emptyView = TextView(this)
            (listView.getParent() as ViewGroup).addView(emptyView)
            emptyView.setText("It's empty!")
            listView.setEmptyView(emptyView)
        }
    }

    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)

        val intent = Intent(this, ExerciseActivityK::class.java)
        intent.putExtra(MethodActivity().INTENT_KEY_METHOD_ID, getIntent().getIntExtra(MethodActivity().INTENT_KEY_METHOD_ID, -1))
        intent.putExtra(VARIANT, variants!![position].idVariant)
        startActivity(intent)
    }
}
