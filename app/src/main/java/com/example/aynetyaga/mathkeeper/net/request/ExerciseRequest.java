package com.example.aynetyaga.mathkeeper.net.request;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

import com.example.aynetyaga.mathkeeper.account.AccountAuthenticator;
import com.example.aynetyaga.mathkeeper.net.response.ExersiceResponse;

import org.ksoap2.serialization.SoapObject;

import com.example.aynetyaga.mathkeeper.utils.Constants;

/**
 * Created by Юлия on 04.03.2016.
 */
public class ExerciseRequest extends BaseRequest {
    private Context context;
    public int idMethod;
    public int variant;

    public ExerciseRequest(Context context) {
        super("GetExercises");

        this.context = context;
    }

    @Override
    public SoapObject getSoapObject() {
        SoapObject object = createSoapObject();
        AccountManager accountManager = AccountManager.get(context);
        Account account = accountManager.getAccountsByType(AccountAuthenticator.ACCOUNT_NAME)[0];
        object.addProperty("idUser", accountManager.getUserData(account, Constants.INSTANCE.getACCOUNT_ID()));
        object.addProperty("idMethod", idMethod);
        object.addProperty("variant", variant);

        return object;
    }

    @Override
    public Class getResponseClass() {
        return ExersiceResponse.class;
    }
}
