package com.example.aynetyaga.mathkeeper.net.request;

import com.example.aynetyaga.mathkeeper.net.response.RegistrationResponse;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Юлия on 20.03.2016.
 */
public class RegistrationRequest extends BaseRequest {
    public String name;
    public String login;
    public String password;

    public RegistrationRequest() {
        super("Registration");
    }

    @Override
    public SoapObject getSoapObject() {
        SoapObject object = createSoapObject();
        object.addProperty("name", name);
        object.addProperty("login", login);
        object.addProperty("password", password);

        return object;
    }

    @Override
    public Class getResponseClass() {
        return RegistrationResponse.class;
    }
}
