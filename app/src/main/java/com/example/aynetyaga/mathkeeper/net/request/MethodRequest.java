package com.example.aynetyaga.mathkeeper.net.request;

import com.example.aynetyaga.mathkeeper.net.response.MethodResponse;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by AYNetyaga on 20.10.15.
 * Rucard LTD
 */
public class MethodRequest extends BaseRequest {
    public MethodRequest() {
        super("GetMethods");
    }

    @Override
    public SoapObject getSoapObject() {
        return createSoapObject();
    }

    @Override
    public Class getResponseClass() {
        return MethodResponse.class;
    }
}
