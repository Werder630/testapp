package com.example.aynetyaga.mathkeeper.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import org.apache.commons.io.output.CountingOutputStream;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;


public class FTPHelperManager {

    private FTPClient mFTPClient;

    private String host;
    private String username;
    private String password;
    private int port;

    private CompleteDownloadListener completeListener;

    public FTPHelperManager() {
        this.host = Constants.INSTANCE.getHost();
        this.username = Constants.INSTANCE.getUsername();
        this.password = Constants.INSTANCE.getPassword();
        this.port = 21;
    }

    public void setDownloadListener(CompleteDownloadListener listener) {
        this.completeListener = listener;
    }

    public void downloadFiles(Context context, int methodId, int variant, List<String> excersices) {
        new InnerDownloder(context, methodId, variant).download(excersices);
    }


    public interface CompleteDownloadListener {
        void complete();
    }

    private class InnerDownloder extends AsyncTask<List<String>, String, CompleteDownloadListener> {

        ProgressDialog progressDialog;
        int methodId;
        int variant;

        InnerDownloder(Context context, int method, int variant) {
            this.methodId = method;
            this.variant = variant;

            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("...");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(true);
        }

        private void download(List<String> excersices) {
            execute(excersices);
        }

        private File getFile() {
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
                    + "/MathKeeper/TR " + methodId + "/Variant " + variant);
            if (!file.exists()) file.mkdirs();
            return file;
        }

        @Override
        protected void onPreExecute() {
            if (!progressDialog.isShowing())
                progressDialog.show();
        }

        @Override
        protected CompleteDownloadListener doInBackground(final List<String>... params) {
            if (connect()) {
                boolean status = false;
                for (final String exercise : params[0]) {
                    try {
                        String filePath = "methods/" + methodId + "/" + variant + "/" + exercise + ".jpg";
                        FTPFile file = mFTPClient.listFiles(filePath)[0];
                        final long size = file.getSize();

                        FileOutputStream desFileStream = new FileOutputStream(getFile() + "/" + exercise + ".jpg");
                        CountingOutputStream cos = new CountingOutputStream(desFileStream) {
                            protected void beforeWrite(int n) {
                                super.beforeWrite(n);

                                publishProgress("" + ((long) (getCount() * 100) / size), exercise);
                            }
                        };

                        status = mFTPClient.retrieveFile(filePath, cos);
                        desFileStream.close();


                    } catch (Exception e) {
                        Log.d("test2", "download failed:" + e.getMessage());
                    }
                }

                if (completeListener != null && status)
                    return completeListener;

                disconnect();

            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            progressDialog.setProgress(Integer.parseInt(values[0]));
            progressDialog.setMessage("Скачивается пример: " + values[1]);
        }

        @Override
        protected void onPostExecute(CompleteDownloadListener completeDownloadListener) {
            if (completeDownloadListener != null) completeDownloadListener.complete();
            progressDialog.dismiss();
        }

        private boolean connect() {
            try {
                mFTPClient = new FTPClient();
                mFTPClient.connect(host, port);

                if (FTPReply.isPositiveCompletion(mFTPClient.getReplyCode())) {
                    boolean status = mFTPClient.login(username, password);

                    mFTPClient.setFileType(FTP.BINARY_FILE_TYPE);
                    mFTPClient.enterLocalPassiveMode();

                    return status;
                }
            } catch (Exception e) {
                Log.d("test2", "Error: could not connect to host:" + e.getMessage());
            }

            return false;
        }

        private void disconnect() {
            try {
                mFTPClient.disconnect();
            } catch (IOException e) {
                Log.d("test2", "Error: could not disconnect:" + e.getMessage());
            }
        }
    }
}
