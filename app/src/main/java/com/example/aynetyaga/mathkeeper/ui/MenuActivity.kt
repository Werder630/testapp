package com.example.aynetyaga.mathkeeper.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.aynetyaga.testapp.R

/**
 * Created by Юлия on 15.03.2016.
 */

class MenuActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.menu_activity);
    }

    fun onExercisesButtonClick(v : View) {
        intent = Intent(this, MethodActivity::class.java)
        startActivity(intent)
    }

    fun onProfileButtonClick(v : View) {
        startActivity(Intent(this, ProfileActivity::class.java))
    }
}
